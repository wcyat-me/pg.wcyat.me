const advanced =
  window.location.href.split("?")[0].endsWith("advanced/") ||
  window.location.href.split("?")[0].endsWith("advanced/index.html");
for (const i in options.include) {
  if (localStorage[i + (advanced ? "-a" : "")] === undefined) {
    localStorage.setItem(i + (advanced ? "-a" : ""), options.include[i]);
  } else {
    options.include[i] = localStorage[i + (advanced ? "-a" : "")] === "true";
  }
}
if (localStorage.getItem(`digits${advanced ? "-a" : ""}`) === null) {
  localStorage.setItem(`digits${advanced ? "-a" : ""}`, options.digits);
} else {
  options.digits = Number(localStorage[`digits${advanced ? "-a" : ""}`]);
}
function selected() {
  for (const i in options.include) {
    document.getElementById(i).checked = options.include[i];
  }
  document.getElementById("digits").value = options.digits;
}
function change(v) {
  o = document.getElementById(v).checked;
  options.include[v] = o;
  localStorage.setItem(v + (advanced ? "-a" : ""), o);
}

function copy() {
  navigator.clipboard.writeText(
    document.getElementById("output")[advanced ? "value" : "innerHTML"]
  );
  document.getElementById("copy").innerHTML = "Copied!";
}

function g() {
  let o = 0;
  for (const i in options.include) {
    o += options.include[i];
  }
  if (
    o > options.digits ||
    o === 0 ||
    options.digits < 0 ||
    (options.digits > 30 && !advanced)
  ) {
    alert(
      "Options invalid. Possible causes: Included items are more than digits / Nothing included / digits is set to smaller than 1 or greater than 30."
    );
  } else {
    document.getElementById("output")[advanced ? "value" : "innerHTML"] =
      generate();
    localStorage[`lastgenerated${advanced ? "-a" : ""}`] =
      document.getElementById("output")[advanced ? "value" : "innerHTML"];
    document.getElementById("copy").innerHTML = "Copy";
  }
}

selected();
if (localStorage[`lastgenerated${advanced ? "-a" : ""}`] === undefined) {
  g();
} else {
  document.getElementById("output")[advanced ? "value" : "innerHTML"] =
    localStorage[`lastgenerated${advanced ? "-a" : ""}`];
}
